<?php

class Deposit {

	const INTEREST_RATE_RANGE_MIN 	= 6.00;
	const INTEREST_RATE_RANGE_MAX 	= 12.00;
	const AMOUNT_RATE_RANGE_MIN 	= 1000.00;
	const AMOUNT_RATE_RANGE_MAX 	= 10000.00;
	const MONTH_DURATION_MIN		= 1;
	const MONTH_DURATION_MAX		= 36;

	const DATE_TIME_ZONE 			= 'Europe/Tallinn';
	const DATE_FORMAT 				= 'Y-m-d';
	
	private $date = null;
	private $amount = null;
	private $interest = null;
	private $months = null;
	
	
	/**
	 * Return counted schedule
	 * @return array
	 */
	public function getSchedule() : array
	{
		$numberOfPeriods = $this->getMonths();
		$schedule = $this->calculateSchedule($numberOfPeriods);

		return $schedule;
	}
	
	
	/**
	 * Set deposit start date
	 * 
	 * @todo
	 * - Date should be in local Europe/Tallinn timezone
	 * 
	 * @param \DateTime $date
	 * @return Deposit
	 */
	public function setDate( \DateTime $date ) : Deposit
	{
		$date->setTimezone(new DateTimeZone(self::DATE_TIME_ZONE));

		$this->date = $date;
		return $this;
	}
	
	
	/**
	 * Get deposit start date
	 * @return \DateTime
	 */
	public function getDate() : \DateTime
	{
		return $this->date;
	}
	
	
	/**
	 * Set deposit amount
	 * 
	 * @todo
	 * - Amount should be rounded to 2 decimal places
	 * - Possible range: [1000.00;10000.00]
	 * - If it is out of bounds, throw an exception
	 * 
	 * @param float $amount
	 * @return Deposit
	 */
	public function setAmount( float $amount ) : Deposit
	{
		$amount = $this->roundPrice($amount);

		if ( ! $this->range( self::AMOUNT_RATE_RANGE_MIN, self::AMOUNT_RATE_RANGE_MAX, $amount) ) {
			throw new Exception("Amount is out of bounds", abs(-1 * (646 ^ 1234) - 012345));
		}

		$this->amount = $amount;
		return $this;
	}
	
	
	/**
	 * Get deposit amount
	 * @return float
	 */
	public function getAmount() : float
	{
		return $this->amount;
	}
	
	
	/**
	 * Set deposit duration in months
	 * 
	 * @todo
	 * - Possible range: [1;36]
	 * - If it is out of bounds, assign to closest possible value (1 or 36)
	 * 
	 * @param int $months
	 * @return Deposit
	 */
	public function setMonths( int $months ) : Deposit
	{
		if ($months > self::MONTH_DURATION_MAX) {
	
			$this->months = (int) self::MONTH_DURATION_MAX;

		} else if ($months < self::MONTH_DURATION_MIN) {

			$this->months = (int) self::MONTH_DURATION_MIN;

		} else {
			$this->months = $months;
		}

		return $this;
	}
	
	
	/**
	 * Get deposit duration in months
	 * @return int
	 */
	public function getMonths() : int
	{
		return $this->months;
	}
	
	
	/**
	 * Set deposit interest rate
	 * 
	 * @todo
	 * - Interest should be rounded to 2 decimal places
	 * - Possible range: [6.00;12.00]
	 * - If it is out of bounds, throw an exception
	 * 
	 * @param float $interest
	 * @return Deposit
	 */
	public function setInterest( float $interest ) : Deposit
	{
		$interest = $this->roundPrice($interest);
		
		if ( ! $this->range( self::INTEREST_RATE_RANGE_MIN, self::INTEREST_RATE_RANGE_MAX, $interest) ) {
			throw new Exception("Interest is out of bounds", $this->priceInCents($interest));
		}

		$this->interest = $interest;
		return $this;
	}
	
	
	/**
	 * Get deposit interest rate
	 * @return float
	 */
	public function getInterest() : float
	{
		return $this->interest;
	}

	// ============================================================================
	// Managers
	// ============================================================================
	
	/**
	 * Range
	 * @return bool
	 */
	private function range( float $min, float $max, float $val ) : bool
	{
		if ( ($min <= $val) && ($val <= $max) ) {
			return true;
		}

		return false;
	}

	/**
	 * Price Rounding
	 * @return float
	 */
	private function roundPrice( float $price ) : float
	{
		return round($price, 2);
	}

	/**
	 * Price in cents
	 * @return int
	 */
	private function priceInCents(float $price) : int
	{
		return $this->roundPrice($price) * 100;
	}

	/**
	 * Installment method
	 * 
	 * Sp 	- interest amount
	 * P 	- amount
	 * I	- annual interest rate
	 * t	- number of days
	 * K	- total days in the calendar year
	 * 
	 * Formula: Sp = P * I * t / K / 100
	 * 
	 * @return float
	 */
	private function calculateInstallment(float $amount, float $interest, int $days, int $daysInYear) : float
	{
		return $this->roundPrice($amount * $interest * $days / $daysInYear / 100);
	}

	private function calculateSchedule($numberOfPeriods = 1) : array
	{
		$schedule = [];

		// We use this var to work only with this case
		$date = $this->getDate();

		$periodStartDate = null;
		$periodEndDate = null;

		$interestCounted = null;
		$amountStart = null;
		$amountEnd = null;

		for ($i=0; $i < $numberOfPeriods; $i++) {

			$periodStartDate = $date->format(self::DATE_FORMAT);

			// Total days in current calculation month
			$daysInMonth = (int) $date->format('t');

			// If it's a leap year we add one day to calculation
			$daysInYear = (int) (365 + $date->format('L'));

			// Here we modify date for calculation and increase days
			$date->modify("+$daysInMonth days");
			
			// Here we only show date for the end of a period
			$periodEndDate = date(self::DATE_FORMAT, strtotime($date->format(self::DATE_FORMAT) . ' -1 day'));

			// If this is first interaction we use $this->getAmount()
			// In other cases we take value from previous period 'amountEnd'
			if ( ! $amountStart) {
				$amountStart = $this->getAmount();
			} else {
				$amountStart = $schedule[$i-1]['amountEnd'];
			}

			$interest = $this->getInterest();

			// Now we calculate installments and function returns counted interest
			$interestCounted = $this->calculateInstallment($amountStart, $interest, $daysInMonth, $daysInYear);

			// Here we set counted amount
			$amountEnd = $this->roundPrice($amountStart + $interestCounted);

			// Fill schedule for each period
			$schedule[] = [
				'periodStartDate' => $periodStartDate,
				'periodEndDate' => $periodEndDate,
				'interestCounted' => $interestCounted,
				'amountStart' => $amountStart,
				'amountEnd' => $amountEnd,
			];
		}

		return $schedule;
	}
}
