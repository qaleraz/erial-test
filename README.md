# Test task for ERIAL jobseeker

Environment used:
 - PHP 7.1
 - Default timezone: UTC
 - strict_types = 1

:grey_exclamation:You are now allowed to change index.php file.
:grey_exclamation:All changes are alowed only in **Deposit.class.php**

You need to update Deposit class to make it satisfy all the conditions and pass the test.
In this class we calculate deposit interests for the specified period.

- By default, date could be passed in UTC timezone, but all calculation are made in Europe/Tallinn;
- Amount is from 1000.00 to 10000.00;
- Deposit duration is from 1 to 36 months;
- Year interest rate is from 6.00% to 12.00%;
- Interest capitalization period is 1 month (that means, each month we add unpaid interest to the principal amount of the deposit);
- Enrolled interest per month depends on days in month and days in year;
- Сalculation formula is unversal and easily found in Google;
- Additional comments are in Deposit.class.php.

There are 3 sub-tests you should pass during this task: two of them are secondary, and the last one related to calculactions is the most important.

In case of any questions please fill free to contact.

Good luck!


----------------------------


# Тестовое задание для соискателя в ERIAL

Используемая среда:
 - PHP 7.1
 - Временная зона по умолчанию: UTC
 - strict_types = 1

:grey_exclamation:Вы не можете делать изменения в файле index.php.
:grey_exclamation:Все изменения разрешены только в **Deposit.class.php**

Необходимо дополнить класс Deposit таким образом, чтоб он удовлетворял всем условиям и прошёл тест.
В классе производится расчёт процентов по вкладу на выбранный период.

- По умолчанию, дата может быть передана во временной зоне UTC, но все расчёты осуществляются в Europe/Tallinn;
- Сумма (amount) может быть от 1000.00 до 10000.00;
- Длительность вклада (duration) от 1 до 36 месяцев;
- Годовая процентная ставка (interest) от 6.00% до 12.00%;
- Период капитализации процентов составляет 1 месяц (это означит, что каждый месяц мы добавляем невыплаченный процент к основной сумме вклада);
- Начисленный процент за месяц зависит от количества дней в месяце и от количества дней в году;
- Формула расчёта универсальная и легко находится в Google;
- Дополнительные комментарии в Deposit.class.php.

Всего надо будет пройти 3 подтеста в процессе выполнения: два из них сопутствующие, и самый последний, который связан с расчётами - самый важный.

В случае каких-либо вопросов, пожалуйста свяжитесь.

Удачи!
